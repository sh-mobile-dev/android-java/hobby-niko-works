package com.niko.nikoworks.Data;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.niko.nikoworks.R;
import com.rubengees.introduction.Slide;

/**
 * Created by Shankar on 17-07-2017.
 */

public class CustomIntro implements Slide.CustomViewBuilder {
    private String title;
    private String description;
    private int imageId;

    public CustomIntro(String title, String description, int imageID) {
        this.title = title;
        this.description = description;
        this.imageId = imageID;
    }

    @NonNull
    @Override
    public View buildView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {

        View introView = inflater.inflate(R.layout.item_intro, parent, false);
        //setting title.
        TextView introTitleTextView = (TextView) introView.findViewById(R.id.introTitleTextView);
        introTitleTextView.setText(title);

        //setting description
        TextView introDescriptionTextView = (TextView) introView.findViewById(R.id.introDescriptionTextView);
        introDescriptionTextView.setText(description);

        //setting image
        ImageView introImageView = (ImageView) introView.findViewById(R.id.introImageView);
        introImageView.setImageResource(imageId);

        return introView;

    }
}

