package com.niko.nikoworks;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.niko.nikoworks.Data.CustomIntro;
import com.niko.nikoworks.Fragments.AboutFragment;
import com.niko.nikoworks.Fragments.ContactUsFragment;
import com.niko.nikoworks.Fragments.FeedbackFragment;
import com.niko.nikoworks.Fragments.WhoFragment;
import com.rubengees.introduction.IntroductionBuilder;
import com.rubengees.introduction.Slide;

import java.util.ArrayList;
import java.util.List;

public class NikoActivity extends AppCompatActivity {

    private String titles[];
    private String descriptions[];
    private int imageIDs[];
    private Fragment fragment;

    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_niko);

        //To show intro on first start of application
        Thread mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                boolean previouslyStarted = preferences.getBoolean(getString(R.string.pref_first_start), false);
                if (!previouslyStarted) {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean(getString(R.string.pref_first_start), true);
                    editor.apply();
                    IntroductionBuilder ib = new IntroductionBuilder(NikoActivity.this);
                    ib.withSlides(prepareSlides());
                    ib.introduceMyself();
                    ib.withPreviousButtonEnabled(true);
                    fragment = new AboutFragment(0);
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_niko, fragment);
                    fragmentTransaction.commit();
                }
                else
                {
                    getstarted();
                }
            }
        });
        mThread.start();
    }

    //creating intro
    private List<Slide> prepareSlides() {
        List<Slide> result = new ArrayList<>();
        imageIDs=new int[]{R.drawable.placeholder,R.drawable.work,R.drawable.internship,R.drawable.training,R.drawable.job};
        titles = getResources().getStringArray(R.array.array_intro_titles);
        descriptions = getResources().getStringArray(R.array.array_intro_descriptions);
        for (int i = 0; i < 5; i++) {
            result.add(new Slide()
                    .withCustomViewBuilder(new CustomIntro(titles[i], descriptions[i], imageIDs[i]))
                    .withColorResource(R.color.md_teal_400)
            );
        }
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.overflow_niko, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.action_about:
                fragment = new AboutFragment(1);
                next();
                return true;
            case R.id.action_contact:
                fragment = new ContactUsFragment();
                next();
                return true;
            case R.id.action_feedback:
                fragment = new FeedbackFragment();
                next();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //displays AboutFragment
    private void next() {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_niko, fragment);
        fragmentTransaction.addToBackStack("Fragment");
        fragmentTransaction.commit();
    }


    //to display MainFragment a.k.a. WhoFragment
    public void getstarted() {
        fragment = new WhoFragment();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_niko, fragment);
        fragmentTransaction.commit();
    }
}
