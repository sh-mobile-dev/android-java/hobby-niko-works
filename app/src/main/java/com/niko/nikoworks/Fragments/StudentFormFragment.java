package com.niko.nikoworks.Fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.niko.nikoworks.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentFormFragment extends Fragment {

    private Button submitButton;
    private Button backButton;
    private EditText nameEditText, emailEditText, phoneEditText, fieldsEditText, extrasEditText;
    private String name, email, phone, fieldsIntrest, extras;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private String mail;

    public StudentFormFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.app_name));
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.app_name));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_form, container, false);
        backButton = (Button) view.findViewById(R.id.backButtonStudentForm);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        nameEditText = (EditText) view.findViewById(R.id.student_name_edittext);
        emailEditText = (EditText) view.findViewById(R.id.student_email_edittext);
        phoneEditText = (EditText) view.findViewById(R.id.student_phone_edittext);
        fieldsEditText = (EditText) view.findViewById(R.id.intrests_edittext);
        extrasEditText = (EditText) view.findViewById(R.id.share_views_edittext);
        radioGroup = (RadioGroup) view.findViewById(R.id.studentRadioGroup);
        submitButton = (Button) view.findViewById(R.id.submitButtonStudentForm);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = nameEditText.getText().toString();
                email = emailEditText.getText().toString();
                phone = phoneEditText.getText().toString();
                fieldsIntrest = fieldsEditText.getText().toString();
                extras = extrasEditText.getText().toString();
                if (name.matches("") || email.matches("") || phone.matches("") || fieldsIntrest.matches("")) {
                    Toast.makeText(getActivity(), getString(R.string.no_entry), Toast.LENGTH_SHORT).show();
                } else {
                    mail = "";
                    mail = "Hi Nik, I am " + name + " i would like to ";
                    int id = radioGroup.getCheckedRadioButtonId();
                    switch (id) {
                        case R.id.radioButtonInternship:
                            mail = mail + "participate in intership programs organized by Niko.Works. ";
                            break;
                        case R.id.radioButtonTraining:
                            mail = mail + "participate in training sessions conducted by Niko.Works. ";
                            break;
                        case R.id.radioButtonJob:
                            mail = mail + "enroll for a job at Niko.Works. ";
                            break;
                        default:
                            Toast.makeText(getActivity(), getString(R.string.no_choice), Toast.LENGTH_SHORT).show();
                    }
                    mail = mail + "my fields of interest are : \n\n" + fieldsIntrest + ".";
                    if (!extras.matches("")) {
                        mail = mail + "\n\nI would like to share something more with you Nik,\n\n" + extras;
                    }
                    mail = mail + "My contact details are provided below. Feel free to contact me.\n\nPhone : " + phone + "\n\nE-Mail : " + email + "\n\nThank You Nik.";
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NEW ASPIRANT APPLICATION : " + name);
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.contact_email)});
                    emailIntent.putExtra(Intent.EXTRA_TEXT, mail);
                    startActivity(emailIntent);
                }
            }
        });
        return view;
    }

}
