package com.niko.nikoworks.Fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.niko.nikoworks.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreferenceCompanyFragment extends Fragment {

    private Button nextButton;
    private RadioGroup radioGroup;
    private RadioButton radioButton, radioButtonOthers;
    private Button backButton;
    private EditText editText;
    String choice;

    public PreferenceCompanyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.app_name));
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.app_name));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_preference_company, container, false);
        radioGroup = (RadioGroup) view.findViewById(R.id.companyPrefRadioGroup);
        editText = (EditText) view.findViewById(R.id.otherField);
        editText.setVisibility(View.GONE);
        //next button press handling
        nextButton = (Button) view.findViewById(R.id.nextButtonCompanyPref);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if (i == R.id.radioButtonOthers) {
                    editText.setVisibility(View.VISIBLE);
                } else {
                    editText.setVisibility(View.GONE);
                }
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean no_sel, oth;
                oth = true;
                no_sel = true;
                int id = radioGroup.getCheckedRadioButtonId();
                if (id == -1) {
                    no_sel = false;
                } else {
                    radioButton = (RadioButton) view.findViewById(id);
                    choice = radioButton.getText().toString();
                    if (choice.equals("Others")) {
                        choice = editText.getText().toString();
                        if (choice.matches(""))
                            oth = false;
                    }
                }
                if (no_sel && oth) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(getString(R.string.pref_comp_field), choice);
                    editor.apply();

                    CompanyFormFragment fragment = new CompanyFormFragment();
                    FragmentTransaction fragmentTransaction;
                    fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_niko, fragment);
                    fragmentTransaction.addToBackStack("Fragment");
                    fragmentTransaction.commit();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.no_choice), Toast.LENGTH_SHORT).show();
                }

            }
        });

        //back button press handling
        backButton = (Button) view.findViewById(R.id.backButtonCompanyPref);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        return view;
    }
}
