package com.niko.nikoworks.Fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.niko.nikoworks.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedbackFragment extends Fragment {

    private EditText nameEditText;
    private EditText phoneEditText;
    private EditText emailEditTexr;
    private EditText feedbackEditText;
    private Button submitFeedback;
    private String name;
    private String feedback;
    private Button backButton;

    public FeedbackFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.feedback));
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        nameEditText = (EditText) view.findViewById(R.id.feedback_name_edittext);
        phoneEditText = (EditText) view.findViewById(R.id.feedback_phone_edittext);
        emailEditTexr = (EditText) view.findViewById(R.id.feedback_email_edittext);
        feedbackEditText = (EditText) view.findViewById(R.id.feedback_edittext);
        submitFeedback = (Button) view.findViewById(R.id.buttonFeedbackSubmit);

        //back button click
        backButton = (Button) view.findViewById(R.id.backButtonFeedbackForm);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        //submit button click
        submitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedback = "";
                name = nameEditText.getText().toString();
                feedback = feedback + "Hey Nik, I'm " + name + "\n\nI would like to share my feedback about Niko.Works :\n" + feedbackEditText.getText() +
                        "\nPhone : " + phoneEditText.getText() + "\nEmail : " + emailEditTexr.getText();
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "FEEDBACK FROM : " + name);
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.contact_email)});
                emailIntent.putExtra(Intent.EXTRA_TEXT, feedback);
                startActivity(emailIntent);
            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}
